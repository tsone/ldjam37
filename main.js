
var SCR_W = 480, SCR_H = 270;
var DIR_DOWN = 0;
var DIR_LEFT = 1;
var DIR_RIGHT = 2;
var DIR_UP = 3;
var GRAVITY = 0.25;
var FLOOR_FRICTION = 0.65;

var THROW_SPEED = 4;
var BOX_VELOCITY = [ [0, 1], [-1, 0], [1, 0], [0, -1] ];

var HELD_ITEM_OFFS = [ [0, 3], [-8, 1], [8, 1], [0, -3] ];

var DROP_HEIGHT = SCR_H - 36;

var body;
var superStage;
var stage;
var actorStage;
var shadowStage;
var matrixFilter;
var allActors = [];
var renderer;
var gamepad;

var activated_keys = [];
var keys = [];

var gamepad_buttons_last = [];

var guy = {};
var thrownItem = null;

var spawnDelay = 120;

var hatches = [];
var score = 0;
var scoreText;
var numBigBlocks = 0;

var gameOver = false;

var texts_a = [];
var texts_b = [];
var lights = [];

function actorSortFunc(a, b) {
  var xa = a.cy*1e9 + a.cz;
  var xb = b.cy*1e9 + b.cz;
  if(xa == xb) return 0;
  return (xa > xb) ? 1 : -1;
}

function endGame()
{
  gameOver = true;
  music.pause();
  dieSound.play();

  flash_t = 0;
  superStage.addChild(guy);
  if(guy.heldItem) superStage.addChild(guy.heldItem);
  superStage.children.sort(actorSortFunc);
  superStage.addChild(writeText(SCR_W * 0.41, SCR_H * 0.5, score, true));
  renderer.render(superStage);
}

function explode(bomb, special)
{
  if(bomb.deleted || !bomb.visible)
    return;
  if(bomb.parent === guy){
    endGame(); 
    return;
  }
  var radius = 120;
  if(!!special)
    radius = 58;
  for(var i = 0; i < actorStage.children.length; i++)
  {
    var child = actorStage.children[i];
    if(child.cx === undefined) continue;
    var v = sub3(child, bomb);
    var dist = len3(v);
    if(dist < radius){
      if(child.isBomb) {
        child.tick += 10 * (radius - dist);//will trigger explosion! 
      }
      else if(dist < 58) {
        child.damage(dist, bomb);
      } 
      if(child.cz <= 0)
        child.cz = 1;
      var force = 30 * Math.pow((radius - dist) / radius, 4.5);
      if(child.weight)
        force *= Math.max(1 / (child.weight / 2) - 0.1, 0.3);
      force = Math.min(force, 5);
      child.vz = 0.5 * force;
      if(!child.isBigBox){
        child.vx += force * v.cx / dist;
        child.vy += force * v.cy / dist;  
      }
    }
  }

  if(!!!special)
    Explosion(bomb);
  explosionSound.play();
  bomb.deleted = true;
  bomb.setVisible(false);
}

function Actor(res, shadow, opt)
{
  opt = opt || {};
  w = opt.w || 32;
  h = opt.h || 32;
  dropped = opt.dropped || false;

  shadow = shadow || "res/shadow-16.png";
  var anims = res ? loadAnim(res, w, h) : null;
  var r = anims ? new PIXI.Sprite(anims[0][0]) : new PIXI.Container();

  r.cx = r.cy = r.cz = 0;
  r.vx = r.vy = r.vz = 0;

  r.shadow = new PIXI.Sprite(PIXI.loader.resources[shadow].texture);
  r.shadow.anchor.set(0.5, 0.5);
  r.tick = 0;
  r.weight = 1;
  r.anims = anims;
  r.animate = true;
  r.animDir = 1;
  r.animIdx = 0;
  r.animFrameIdx = 0;
  r.animTick = 0;
  r.animDelay = 4;
  r.dropDelay = dropped ? spawnDelay/4 : 0;

  r.deleted = false;
  r.physics = true;
  r.gravity = true;
  r.collides = true;
  r.collides_level = true;
  r.coll = { w:5, h:3, d:4 };

  r.colliding = null;
  r.dir = DIR_DOWN;

  r.setAnimIdx = function(idx) {
    r.animTick = 0;
    r.animIdx = idx;
    if(r.anims) {
      r.texture = r.anims[r.animIdx][r.animFrameIdx];
    }
  };

  r.updatePhysics = function() {
    if(!r.physics) return;

    var oldX = r.cx;
    var oldY = r.cy;
    if(r.gravity && r.cz > 0) {
      r.vz -= GRAVITY;
    }
    r.cx += r.vx;
    r.cy += r.vy;
    var prevZ = r.cz;
    r.cz += r.vz;
    if (r.cz <= 0) {
      if(prevZ > 0 && Math.abs(r.vz) > GRAVITY){
        if(r.thrown && r.isBomb){ r.tick += 60; }
        if(r.isBigBox){
          landSound.play();
        }
        r.vz = -r.vz * 0.15;
      }
      r.vx *= FLOOR_FRICTION;
      r.vy *= FLOOR_FRICTION;
      r.cz = 0;
      if(thrownItem && r === thrownItem && (r.vx*r.vx + r.vy*r.vy) < 4) {
        thrownItem = null;
      }
    }
 
    if(!r.collides || (oldX == r.cx && oldY == r.cy)) return;
    if(r.collides_level) {
      Level.constrain(r, oldX, oldY);
    }

    r.colliding = null;
    for(var i = 0; i < allActors.length; ++i) {
      var o = allActors[i];
      if(!o.collides || (o === r)) continue;

      if(r.hits(o)) {
        r.colliding = o;
        if(o.gravity) {
          r.cx = oldX;
          r.cy = oldY;
          r.vx = r.vy = r.vz = 0;
        } else if(r === thrownItem) {
          r.vx *= 0.5;
          r.vy *= 0.5;
          r.vz *= 0.5;
        }
      }
    }
  };

  r.setVisible = function(vis) {
    r.visible = vis;
    r.shadow.visible = vis;
  };

  r.onUpdate = null;
  r.update = function() {
    ++r.tick;

    --r.dropDelay;
    if(r.dropDelay <= 0) {
      r.updatePhysics();
      r.dropDelay = 0;
    }

    r.position.set(r.cx|0, (r.cy|0)-(r.cz|0));

    if(r.shadow.visible) {
      r.shadow.position.set(r.cx|0, r.cy|0);
    }

    if(r.dropDelay > 0) return;

    if(r.onUpdate) r.onUpdate();

    if(r.animate){
      ++r.animTick;
      if(r.anims && (r.animDelay >= 0) && (r.animTick >= r.animDelay)) {
        r.animFrameIdx = (r.anims[r.animIdx].length + (r.animFrameIdx+r.animDir)) % r.anims[r.animIdx].length;
        r.setAnimIdx(r.animIdx);
      }
    }

    if(r.isBomb){
      r.animDelay = 13 - 4 * (r.tick / 100);
    }
    if(r.isBomb && r.visible && r.tick > 300) {
      explode(r);
    }
  };

  r.destroy = function() {
    r.deleted = true;
    r.setVisible(false);
  };

  r.onDamage = null;
  r.damage = function(dmg, bomb) {
    if(r.onDamage) r.onDamage(dmg, bomb);
    // TODO: something here?
  };

  r.onRemove = null;
  r.remove = function() {
    if(r.onRemove) r.onRemove();
    if(r.parent) {
      r.parent.removeChild(r);
    }
    allActors.erase(r);
  }

  r.hits = function(o, extend) {
    extend = extend || 0;
    var d = sub3(o, r);
    abs3(d);
    return (d.cx < r.coll.w+o.coll.w+extend)
      && (d.cy < r.coll.h+o.coll.h+extend)
      && (d.cz < r.coll.d+o.coll.d+extend);
    //return (d.cx < r.coll.w+o.coll.w) && (d.cy < r.coll.h+o.coll.h);
  };

  shadowStage.addChild(r.shadow);
  actorStage.addChild(r);
  allActors.push(r);
  return r; 
}

function Flying()
{
  var r = Actor("res/enemy-flying.png", "res/skull-shadow.png", { dropped:1 } );
  r.anchor.set(0.5, 0.5);
  r.coll = { w:7, h:7, d:7 };
  r.gravity = false;
  r.collides_level = false;
  r.dropDelay = 0;
  r.weight = 1;
  r.state = 0;

  r.onUpdate = function() {
    if(r.hits(guy, 1)) {
      guy.damage(1);
    }
    if(thrownItem && r.hits(thrownItem, 2)) {
      r.damage(1);
    }

    var d = sub3(guy, r);
    var n = copy3(d);
    norm3(n);

    switch(r.state){
    case 0:
      // finding attack position
      var g = 2 * (0.8+0.2*Math.sin(2*3.14*r.tick / 180));
      r.vx = g * n.cx;
      r.vy = g * n.cy;
      r.cz = 100;
      if(len2(d) > 96) break;
      // transition
      r.tick = 0;
      r.state = 1;
      // ->
    case 1:
      // aiming
      r.vx *= 0.9;
      r.vy *= 0.9;
      r.vz *= 0.9;
      if(r.tick < 60) break;
      // transition
      r.physics = false;
      r.tick = 0;
      r.start = copy3(r);
      r.end = { cx:r.cx+2*d.cx, cy:r.cy+2*d.cy, cz:100 };
      r.state = 2;
      // ->
    case 2:
      // attacking
      var t = r.tick / 120;
      r.cx = lerp(r.start.cx, r.end.cx, t);
      r.cy = lerp(r.start.cy, r.end.cy, t);
      var s = 2*t - 1;
      var f = s * s;
      f = (1-s)*f + s*(0.5+0.5*Math.cos(2*3.14*t));
      r.cz = f * 95 + 5;
      r.vx = 3 * (1-f); // for animation
      if(t < 1) break;
      // transition
      r.physics = true;
      r.tick = 0;
      r.state = 3;
      // ->
    case 3:
      // puzzled
      r.vx *= 0.9;
      r.vy *= 0.9;
      r.vz *= 0.9;
      if(r.tick < 120) break;
      r.tick = 0;
      r.state = 0;
      break;
    }


    var vlen = Math.sqrt(r.vx*r.vx + r.vy*r.vy);
    r.animDelay = (8 / (0.5+vlen)) |0;
  };

  r.onDamage = function(dmg, bomb) {
    if(dmg < 24) {
      getPoints(100, r.cx, r.cy - 10);
      Explosion(r, true)
      r.destroy();
    }
  };

  return r;
}

function Skull(special)
{
  var r = Actor((special) ? "res/enemy-skull2.png" : "res/enemy-skull.png", "res/skull-shadow.png", { dropped:1 } );
  r.special = special;
  r.anchor.set(0.5, 0.83);
  r.weight = 1;

  r.collideTick = 1e12;
  r.collidingMemo = null;

  r.onUpdate = function() {
    if(r.hits(guy, 1)) {
      guy.damage(1);
    }

    var speed = (r.special) ? 1.3 : 0.8;
    var g = speed * (0.57+0.43*Math.sin(2*3.14*r.tick / 60));

    if(r.colliding) {
      r.collideTick = 0;
      r.collidingMemo = copy3(r.colliding);
    }

    var d;
    if(r.collideTick < 60) {
      ++r.collideTick;
      d = sub3(r, r.collidingMemo);
      var dc = copy3(d);
      perp2(dc);
      if(r.tick % 200 >= 100) inv3(dc);
      mulScalar3(d, 0.5);
      d = add3(d, dc);
      g *= 2;
    } else { 
      r.collideTick = 1e12;
      d = sub3(guy, r);
    }
 
    norm3(d);
    r.vx = g * d.cx;
    r.vy = g * d.cy;

    var vlen = Math.sqrt(r.vx*r.vx + r.vy*r.vy);
    r.animDelay = (3 / (0.25+vlen)) |0;
  };

  r.onDamage = function(dmg, bomb) {
    if(dmg < 34) {
      if(bomb !== undefined && !!bomb.thrown)
        getPoints(200, r.cx, r.cy - 0);
      else
        getPoints(50, r.cx, r.cy - 0);
      Explosion(r, true)
      r.destroy();
    }
  };

  return r;
}

function Hatch(x, y, open)
{
  var r = Actor("res/hatch.png", undefined, { w:20, h:24 });
  r.removeChild(r.shadow);
  delete(r.shadow);
  r.shadow = {visible:false};
  r.x = x; r.y = y;
  r.cx = x;
  r.cy = y;
  r.anchor.set(0.0, 0.0);
  r.open = (open !== undefined) ? open : true;
  if(r.open){
    r.animFrameIdx = r.anims[0].length - 1;
    r.setAnimIdx(0);
  }
  r.animate = false;
  r.animDelay = 5;
  //r.text = new PIXI.Text('open',{fontFamily : '', fontSize: 16, fill : 0x2666EA, align : 'center'});
  //r.text.anchor.set(0.5,0.5);
  //r.addChild(r.text);

  r.lastChange = 0;
  r.delay = 0;
  r.triggered = 0;

  r.triggerStateChange = function()
  {
    if(r.triggered) return;
    r.triggered = true;
    r.delay = 0;
    for(var j = 0; j < r.lights.length; j++)
      r.lights[j].animate = true;
  }

  var setAnim = r.setAnimIdx;
  r.setAnimIdx = function(i){
    if(r.animFrameIdx == 0 || r.animFrameIdx == r.anims[0].length - 1){
      r.animDir = 0;
      r.animate = false;
      for(var j = 0; j < r.lights.length; j++){
        r.lights[j].animate = false;
        r.lights[j].animFrameIdx = (r.open) ? 1 : 0;
        r.lights[j].setAnimIdx(0);
      }
    }
    r.open = r.animFrameIdx > 2;
    return setAnim(i);
  }

  r.onUpdate = function()
  {
    if(r.triggered && r.delay++ > 120){
      r.triggered = false;
      r.delay = 0;
      if(!r.broken && !r.animate){
        r.lastChange = guy.tick;
        r.animate = true;
        r.animTick = 0;
        r.animDir = (r.open) ? -1 : 1;
        r.open = true;
      }
    }
  };
  
  hatches.push(r);
  return r;
}

function Light(x, y)
{
  var r = Actor("res/light.png", undefined, { w:8, h:11 });
  r.removeChild(r.shadow);
  delete(r.shadow);
  r.shadow = {visible:false};
  r.x = x; r.y = y;
  r.cx = x;
  r.cy = y;
  r.animate = false;
  r.anchor.set(0.0, 0.0);
  r.animDelay = 11;
  r.animFrameIdx = 1;
  r.setAnimIdx(0);
  stage.addChild(r);
  lights.push(r);
  return r;
}

function Prop(res, pos, opt)
{
  var r;
  if(opt && opt.h)
    r = Actor(res, "res/shadow-16.png", opt);
  else
    r = Actor(res);
  r.cx = pos.cx;
  r.cy = pos.cy;
  r.cz = pos.cz;
  r.physics = false;
  r.collides = false;
  r.shadow.visible = false;
  r.anchor.set(0.5, 0.5);
  return r;
}

function Explosion(pos, short)
{
  pos.cy = pos.cy - 8;
  var r = Prop("res/explosion.png", pos, { w:48, h:48 } );
  r.animDelay = 3;
  r.onUpdate = function() {
    if(!!short && r.tick > 4 * 3)
      r.destroy();
    else if(r.tick >= 11*3) {
      r.destroy();
    }
  };
  return r;
}

function keyDown(key)
{
  if(key.charCodeAt)
    key = key.charCodeAt(0);
  return !!keys[key];
}

function buttonDown(button)
{
  if(gamepad && button < gamepad.buttons.length)
    return gamepad.buttons[button].pressed;
  return false;
}

function keyActive(key)
{
  if(key.charCodeAt)
    key = key.charCodeAt(0);
  return !!activated_keys[key];
}

function buttonActive(button)
{
  if(gamepad && button < gamepad.buttons.length)
    return !!!gamepad_buttons_last[button] && gamepad.buttons[button].pressed;
  return false;
}

function rgba(r, g, b, a)
{
  return PIXI.utils.rgb2hex([r, g, b]);
  //return "rgba("+Math.round(r*255)+", "+Math.round(g*255)+", "+Math.round(b*255)+", "+a+")";
}

function random(min, max)
{
  if(min === undefined){
    min = 0;
    max = 1;
  }
  if(max === undefined){
    max = min;
    min = 0;
  }

  return (Math.random() * (max - min)) + min;
}

function lerp(a, b, f)
{
  return a + (b - a) * f;
}

function pick(array,erase)
{
  if(erase){
    var i = Math.round(random(0, array.length - 1));
    return array.splice(i, 1)[0]; 
  }
  return array[Math.round(random(0, array.length - 1))];
}

function average(array)
{
  if(array.length == 0) return 0;
  var sum = 0;
  for(var i = 0; i < array.length; i++)
    sum += array[i];
  return sum / array.length;
}

function len(array, b)
{
  if(b !== undefined)
    return Math.sqrt(Math.pow(array, 2) + Math.pow(b, 2))  
  if(array.x !== undefined)
    return Math.sqrt(Math.pow(array.x, 2) + Math.pow(array.y, 2))
  return Math.sqrt(Math.pow(array[0], 2) + Math.pow(array[1], 2))
}

function lerp3(a, b, t) {
  var it = 1-t;
  return { cx:it*a.cx + t*b.cx, cy:it*a.cy + t*b.cy, cz:it*a.cz + t*b.cz };
}

function copy3(v) {
  return { cx:v.cx, cy:v.cy, cz:v.cz };
}

function len2(v) {
  return Math.sqrt(v.cx*v.cx + v.cy*v.cy);
}

function len3(v) {
  return Math.sqrt(v.cx*v.cx + v.cy*v.cy + v.cz*v.cz);
}

function add3(a, b) {
  return { cx:a.cx+b.cx, cy:a.cy+b.cy, cz:a.cz+b.cz };
}

function sub3(a, b) {
  return { cx:a.cx-b.cx, cy:a.cy-b.cy, cz:a.cz-b.cz };
}

function mulScalar3(v, s) {
  v.cx *= s;
  v.cy *= s;
  v.cz *= s;
}

function inv3(v) {
  v.cx = -v.cx;
  v.cy = -v.cy;
  v.cz = -v.cz;
}

function perp2(v) {
  var t = v.cx;
  v.cx = -v.cy;
  v.cy = t;
}

function dist3(a, b) {
  return len3(sub3(a, b));
}

function abs3(v) {
  v.cx = Math.abs(v.cx);
  v.cy = Math.abs(v.cy);
  v.cz = Math.abs(v.cz);
}

function divScalar2(v, s) {
  if(s != 0) {
    v.cx /= s;
    v.cy /= s;
  } else {
    v.cx = v.cy = 0;
  }
}

function divScalar3(v, s) {
  if(s != 0) {
    v.cx /= s;
    v.cy /= s;
    v.cz /= s;
  } else {
    v.cx = v.cy = v.cz = 0;
  }
}

function norm2(v) {
  divScalar2(v, len2(v));
}

function norm3(v) {
  divScalar3(v, len3(v));
}

var gamepads;
var animframes = [], carryframes = [], bombframes = [];
var animTick = 0;

var items = [];

function getPoints(value, x, y)
{
  scoreSound.play();
  score += value;
  var text_a = writeText(x - 8, y, value, true, 1);
  text_a.tick = 0;
  stage.addChild(text_a);
  texts_a.push(text_a);
  var text_b = writeText(x - 8, y, value, true, 2);
  text_b.tick = 0;
  stage.addChild(text_b);
  texts_b.push(text_b);
}

function scoreBox(box, index, x, y){
  if(box.scoreValue){
    getPoints(box.scoreValue, x, y);
  }
  box.setVisible(false);
  items.splice(index, 1);
  box.destroy();
}

function updateCharacter()
{
  var dir = -1;
  var movement = { x:0, y:0 };
  if(gamepad && gamepad.connected) {
    var v = gamepad.axes;
    movement.x = (Math.abs(v[0]) >= 0.2) ? v[0] : 0;
    movement.y = (Math.abs(v[1]) >= 0.2) ? v[1] : 0;
    var ax = Math.abs(movement.x);
    var ay = Math.abs(movement.y);
    if((ax > ay) && (ax >= 0.2)) {
      if(movement.x < 0) dir = DIR_LEFT;
      else dir = DIR_RIGHT;
    } else if((ax < ay) && (ay >= 0.2)) {
      if(movement.y > 0) dir = DIR_DOWN;
      else dir = DIR_UP;
    }
  }

  //keyboard
  if(keyDown('W') || keyDown(38) || buttonDown(12)) {
    movement.y = -1;
    dir = DIR_UP;
  }
  if(keyDown('A') || keyDown(37) || buttonDown(14)) {
    movement.x = -1;
    dir = DIR_LEFT;
  }
  if(keyDown('S') || keyDown(40) || buttonDown(13)) {
    movement.y = 1;
    dir = DIR_DOWN;
  }
  if(keyDown('D') || keyDown(39) || buttonDown(15)) {
    movement.x = 1;
    dir = DIR_RIGHT;
  }
  if(dir >= 0) guy.dir = dir;

  var heldItem = guy.heldItem;

  var newX = 0;
  var newY = 0;

  var length = len(movement);
  if(length > 0.0){
    movement.x /= length;
    movement.y /= length;
    var speed = (heldItem && heldItem.weight) ? 2.5 - heldItem.weight / 2 : 2.5;
    newX = speed * movement.x;
    newY = speed * movement.y;
  }

  guy.vx = lerp(newX, guy.vx, 0.85);
  guy.vy = lerp(newY, guy.vy, 0.85);

  var rate = Math.round(len(guy.vx, guy.vy));
  if(guy.tick % Math.max(5 - rate, 1) == 0){
    animTick = (animTick + 1) % 6;
    if(len(guy.vx, guy.vy) < 0.05)
      animTick = 0;
  }

  var tickmap = [0, 1, 1, 2, 3, 3];
  var animTick_mapped = tickmap[animTick];

  if(heldItem)
    guy.sprite.texture = (carryframes[guy.dir][animTick_mapped]);
  else
    guy.sprite.texture = (animframes[guy.dir][animTick_mapped]);

  if(keyActive(32) || keyActive('Z') || buttonActive(0)){
    if(heldItem){
      heldItem.vx = THROW_SPEED * BOX_VELOCITY[guy.dir][0];
      heldItem.vy = THROW_SPEED * BOX_VELOCITY[guy.dir][1];
      heldItem.vz = 0;
      heldItem.thrown = true;
      thrownItem = heldItem;
      heldItem.physics = true;
      heldItem.collides = true;
      items.push(heldItem);
      guy.heldItem = null;
    }
    else{
      var n = -1;
      var mindist = 10;
      for(var i = 0; i < items.length; i++){
        if(!items[i].visible || items[i].deleted)
          continue;
        var dist = dist3(guy, items[i]);
        if(dist < mindist){
          mindist = dist;
          n = i;
        }
      }

      if(n != -1){
        items[n].physics = false;
        items[n].collides = false;
        thrownItem = null;
        guy.heldItem = items[n];
        items.splice(n, 1);
      }  
    }
  }

  for(var i = 0; i < items.length; i++){
    var box = items[i];
    if(box.cz == 0) {
      for(var j = 0; j < hatches.length; j++){
        if(!!!hatches[j].open)
          continue;
        var w = hatches[j].texture.width;
        var h = hatches[j].texture.height;
        var dist = len(box.cx - (hatches[j].x + w / 2), box.cy - (hatches[j].y + h));
        if(dist < 10){
          if(box.isBomb){
            hatches[j].open = false;
            hatches[j].animFrameIdx = 0; 
            hatches[j].setAnimIdx(0);
            hatches[j].broken = true;
            hatches[j].texture = PIXI.loader.resources["res/hatch-broken.png"].texture
            explode(box, true);
          }
          scoreBox(box, i, hatches[j].x + w / 2, hatches[j].y);
          i--;
          break;
        }
      }
    }
  }

  var oldX = guy.cx;
  var oldY = guy.cy;
  guy.cx += guy.vx;
  guy.cy += guy.vy;
  Level.constrain(guy, oldX, oldY);
}

var spawnPoints = [[40,100],[104,164],[168,132],[134,230],[232,196],[164,132],[296,100],[40,100],[360,132],[328,196],[296,230]];

function pickSpawn()
{
  var available = [];
  for(var i = 0; i < spawnPoints.length; i++){
    var good = true;
    for (var j = allActors.length - 1; j >= 0; j--) {
      if(len(allActors[j].cx - spawnPoints[i][0], allActors[j].cy - spawnPoints[i][1]) < 10){
        good = false;
        break;
      }
    }
    if(good)
      available.push(spawnPoints[i]);
  }

  if(available.length == 0){
    return [random(0.25 * SCR_W, 0.75 * SCR_W), random(0.25 * SCR_H, 0.75 * SCR_H) ];
  }

  var spawn = pick(available);
  spawn[0] += random(-5, 5);
  spawn[1] += random(-5, 5);
  return spawn;
}

function addBomb(x, y)
{
  var bomb = Actor("res/bomb.png", "res/bomb-shadow.png", { dropped:1 } );

  var spawn = pickSpawn();
  var x = spawn[0];
  var y = spawn[1];
  bomb.cx = x;
  bomb.cy = y;
  bomb.cz = DROP_HEIGHT;
  bomb.scoreValue = 0;
  bomb.weight = 1;
  bomb.anchor.set(0.5, 0.75);
  bomb.vx = bomb.vy = bomb.vz = 0;
  bomb.coll = { w:5, h:3, d:4 };
  items.push(bomb);
  bomb.isBomb = true; 
}

var add_flying_i = 0;
function addFlying()
{
   var enemy = Flying();
   enemy.cx = (2*64+SCR_W-96) * (add_flying_i&1) - 64;
   enemy.cy = SCR_H;
   enemy.cz = 100;
   ++add_flying_i;
}

function addSkull(special)
{
  var enemy = Skull(special);
  var spawn = pickSpawn();
  var x = spawn[0];
  var y = spawn[1];
  enemy.cx = x;
  enemy.cy = y;
  enemy.cz = DROP_HEIGHT;
}

function addBox(special)
{
  var sprite = (special) ? "res/smallbox2.png" : "res/smallbox.png";
  var box = Actor(sprite, "res/smallbox-shadow.png", { dropped:1 });

  var spawn = pickSpawn();
  var x = spawn[0];
  var y = spawn[1];
  box.cx = x;
  box.cy = y;
  box.weight = (special) ? 3.5 : 2;
  box.scoreValue = (special) ? 500 : 100;
  box.cz = DROP_HEIGHT;
  box.anchor.set(0.5, 0.825);
  box.vx = box.vy = box.vz = 0;
  box.coll = { w:5, h:5, d:4 };
  items.push(box);
}

var bigBlockSpots = [[56,177],[120,209],[280,209],[184,192],[200,166],[314,144]];

function addBigBlock()
{
  var box = Actor("res/bigbox.png", "res/bigbox-shadow.png", { w:32, h:59, dropped:1 } );

  var spawn = pick(bigBlockSpots, true);
  var x = spawn[0];
  var w = box.texture.width;
  var h = box.texture.height; 
  x -= w / 2;
  var y = spawn[1];
  y -= h;
  box.cx = x;
  box.cy = y;
  box.cz = DROP_HEIGHT; 
  box.weight = 5;

  box.anchor.set(0.5, 0.5);
  box.vx = box.vy = box.vz = 0;
  box.coll = { w:5, h:5, d:4 };//
  box.isBigBox = true;
  box.landed = false;
  box.onUpdate = function(){
    if(!box.landed && box.cz == 0){
      box.landed = true;

      var tx = Math.floor((x-w/2-Level.container.x) / Level.S);
      for(var i = 1; i < 3; i++){
        var ty = Math.floor((y+h/2-Level.container.y) / Level.S) - i;
        Level.tilemap[ty*Level.width+tx] = 
        Level.tilemap[ty*Level.width+tx+1] = 9;
      }
      Level.update();
      
      //smash!!
      for(var i = 0; i < allActors.length; i++){
        if(allActors[i].cz != 0)
          continue;
        if(allActors[i].isBigBox)
          continue;
        if(Math.abs(allActors[i].cx - x) <= w / 2 && Math.abs(allActors[i].cy - y) <= h / 2){
          if(allActors[i].isBomb)
            explode(allActors[i]);
          allActors[i].deleted = true;
          if(allActors[i] === guy)
            endGame();
        }
      }
    }
  };
}

function updateInput()
{
  activated_keys = [];

  if(gamepad){
    for(var i = 0; i < gamepad.buttons.length; i++){
      gamepad_buttons_last[i] = gamepad.buttons[i].pressed;
    }
  }
}

var digit_bg;
var flashDigits;
var goldDigits;
var digits;
var flash_t = 0;
var spawn_tick = 0;

function setText(container, value, center, special)
{
  container.removeChildren();
  var array = "" + value;
  var color = ((value >= 1000) || special) ? (special == 2 ? flashDigits : goldDigits) : digits;
  for(var i = 0; i < array.length; i++){
      var x = (!!center) ? (i - array.length/2) * 12 : i * 12;
    if(!special){
      var sprite_bg = new PIXI.Sprite(digit_bg[0][0]);
      container.addChild(sprite_bg);
      sprite_bg.position.set(x, 0);
    }
    var sprite = new PIXI.Sprite(color[0][parseInt(array[i], 10)]);
    container.addChild(sprite);
    sprite.position.set(x, 0);
  }
}

function writeText(x, y, value, center, special)
{
  var container = new PIXI.Container();
  container.position.set(x, y);
  setText(container, value, center, special);
  return container;
}

function reset()
{
  if(!music.playing)
    music.play();

  Spawn.reset();

  score = 0;
  gameOver = 0;
  guy.tick = 0;
  allActors = [];
  hatches = [];
  items = [];
  lights = [];

  //Create a container object called the `stage`
  superStage = new PIXI.Container();
  superStage.x = 40;
  stage = new PIXI.Container();
  actorStage = new PIXI.Container();
  shadowStage = new PIXI.Container();

  matrixFilter = new PIXI.filters.ColorMatrixFilter();
  stage.filters = [ matrixFilter ];

  var mockup = new PIXI.Sprite(PIXI.loader.resources["res/env-mockup-3.png"].texture);
  stage.addChild(mockup);

  Level.init(stage);
  Level.update(); // TODO: temp
  Level.setPos(-8, 48);

  //create sprite
  guy = Actor();
  guy.weight = 1.5;
  guy.dropDelay = 0;
  addBox();

  guy.coll = { w:5, h:4, d:4 };
//  guy.anchor.set(0.5, 0.83);
  guy.cx = SCR_W / 2;
  guy.cy = SCR_H * 0.5;

  guy.sprite = new PIXI.Sprite(animframes[0][0]); 
  guy.sprite.anchor.set(0.5, 0.83);
  guy.onDamage = function(dmg) {
    if(dmg < 24) {
      endGame();
    }
  };
  guy.onUpdate = function() {
    var heldItem = guy.heldItem;
    if(heldItem) {
      heldItem.cx = guy.cx + HELD_ITEM_OFFS[guy.dir][0];
      heldItem.cy = guy.cy + HELD_ITEM_OFFS[guy.dir][1];
      heldItem.cz = guy.cz + 10;
    }
  };
  
  guy.addChild(guy.sprite);

  var lightYpos = 56;
  var lightXpos = [ 20, 52, 84, 116, 148, 180, 212, 244, 276, 308, 340, 372 ];
  for(var i = 0; i < lightXpos.length; i++){
    Light(lightXpos[i], lightYpos);
  }

  var hatchYpos = 44;
  var hatchXpos = [ 30, 62, 94, 126, 158, 222, 254, 286, 318, 350 ];
  for(var i = 0; i < hatchXpos.length; i++){
    var hatch = Hatch(hatchXpos[i], hatchYpos);
    stage.addChild(hatch);

    if(i < 5){
      hatch.lights = [lights[i], lights[i+1]];
    }
    else{
      hatch.lights = [lights[i+1], lights[i+2]];
    }
  }

  stage.addChild(shadowStage);
  stage.addChild(actorStage);
  superStage.addChild(stage);

  scoreText = writeText(2, 2, 0, false);
  stage.addChild(scoreText);

  //Tell the `renderer` to `render` the `stage`
  renderer.render(superStage);
}

var ACT_BOX    = 0;
var ACT_BOX2   = 1;
var ACT_BOMB   = 2;
var ACT_SKULL  = 3;
var ACT_FLYING = 4;
var ACT_BIGBOX = 5;
var ACT_SKULL2 = 6;

var SPAWNS = [
/*
import random;a=[0,0,0,0,0,0,0,0, 2,2,2,2,2,2,2,2, 3,3,3, 4,4, 5,5, 1,1];random.shuffle(a);print a
*/
[2, 0, 5, 3, 2, 0, 1, 0, 3, 2, 0, 0, 0, 2, 4, 3, 2, 1, 2, 0, 2, 0, 2, 5, 4],
/*
import random;a=[0,0,0,0, 1,1,1, 2,2,2,2,2,2,2, 3,3,3, 4,4,4, 5,5,5, 6,6];random.shuffle(a);print a
*/
[1, 2, 4, 1, 2, 6, 5, 1, 2, 2, 0, 2, 3, 0, 6, 4, 0, 3, 3, 0, 5, 2, 4, 5, 2],
];

var Spawn = {
idx : 0,
seqIdx : 0,
limit : 2,
count : 0,

reset : function() {
  Spawn.idx = 0;
  Spawn.seqIdx = 0;
  Spawn.limit = 2;
  Spawn.count = 0;
},

seq : function() {
  return SPAWNS[Spawn.seqIdx];
},

doit : function() {
  ++Spawn.count;

  var id = Spawn.seq()[Spawn.idx];
  if(id >= Spawn.limit) {
    id = 0;
  }
  //id = 2 + (Spawn.count % 2);
  //id = Spawn.count % 7;

  Spawn.idx = (Spawn.idx + 1) % Spawn.seq().length;

  switch(id) {
  case 0: addBox(0); break;
  case 1: addBox(1); break;
  case 2: addBomb(); break;
  case 3: addSkull(0); break;
  case 4: addFlying(); break;
  case 5: addBigBlock(); break;
  case 6: addSkull(1); break;
  }
},

update : function() {
  if(Spawn.count >= 34) {
    Spawn.limit = 7;
    spawnDelay = 60;
  }
  else if(Spawn.count >= 21) {
    Spawn.limit = 6;
    Spawn.seqIdx = 1;
    spawnDelay = 75;
  }
  else if(Spawn.count >= 13) {
    Spawn.limit = 5;
    spawnDelay = 90;
  }
  else if(Spawn.count >= 3) {
    Spawn.limit = 4;
    spawnDelay = 105;
  }
},
};


function updateHatches()
{
   for(var j = 0; j < lights.length; j++){
    if(!lights[j].animate){
      lights[j].animFrameIdx = 0;
      lights[j].setAnimIdx(0);  
    }
  }

  var t = guy.tick;
  if(t % 300 != 0) return;

  var time = 60 * 60 * 3;
  var targetHatchesOpen = Math.round(Math.max(2, 9 * Math.pow((time - t) / time, 2.0)));
  targetHatchesOpen = Math.max(2, Math.min(10, Math.round(targetHatchesOpen + random(-1, 1))));

  var open = [];
  var openable = [];
  var special = [];
  for(var j = 0; j < hatches.length; j++){
    if(!hatches[j].broken && hatches[j].lastChange > 60 * 30){
      special.push(hatches[j]);
    }
    if(hatches[j].open){
      open.push(hatches[j]);
    }
    else if(!hatches[j].broken)
      openable.push(hatches[j]);
  }

  if(special.length > 0 && open.length == targetHatchesOpen){
    var hatch = pick(special);
    hatch.triggerStateChange();
  }
  else if(open.length > 0 && open.length > targetHatchesOpen){
    var hatch = pick(open);
    if(t - hatch.lastChange < 300) return;
    hatch.triggerStateChange();
  }
  else if(openable.length > 0){
    var hatch = pick(openable);
    if(t - hatch.lastChange < 300) return;
    hatch.triggerStateChange();
  }
}

var paused = false;

function gameLoop() {
  //Loop this function at 60 frames per second
  requestAnimationFrame(gameLoop);

  gamepad = (navigator.getGamepads) ? navigator.getGamepads()[0] : (navigator.getGamepadsWebkit) ? navigator.getGamepadsWebkit()[0] : undefined;

  flash_t++;
  spawn_tick++;

  if(gameOver){
    var t = Math.pow(Math.min(flash_t / 400, 1), 0.45);
    var v = Math.floor(0.5 + 31.0 * (1-t)) / 31.0;
    var matrix = matrixFilter.matrix;
    matrix[0] = matrix[6] = matrix[12] = v;
    renderer.render(superStage);

    if(keyActive(13) || buttonActive(9) || buttonActive(11)){
      reset();
    }
    updateInput();
    return;
  }

  if(keyActive('P'))
    paused = !paused;
  if(paused) {
    updateInput();
    return;
  }


  if(keyActive('M')){
    if(music.volume != 0){
      for(var i = 0; i < effects.length; i++)
        effects[i].volume = 0.0;
      music.volume = 0.0;
    }
    else{
      for(var i = 0; i < effects.length; i++)
        effects[i].volume = 0.5;
      music.volume = 0.5;
    }
  }

  updateHatches();

  for(var i = 0; i < texts_a.length; i++){
    texts_a[i].visible = (((flash_t / 3) & 1) == 0);
    texts_a[i].position.y -= 0.5;
    if(texts_a[i].tick++ > 60){
      stage.removeChild(texts_a[i]);
      texts_a.splice(i, 1);
      i--;
    }
  }
  for(var i = 0; i < texts_b.length; i++){
    texts_b[i].visible = (((flash_t / 3) & 1) != 0);
    texts_b[i].position.y -= 0.5;
    if(texts_b[i].tick++ > 60){
      stage.removeChild(texts_b[i]);
      texts_b.splice(i, 1);
      i--;
    }
  }

  setText(scoreText, score);

  if(spawn_tick > 3*spawnDelay/4 && items.length < 20){
    Spawn.doit();
    spawn_tick = 0;
  }
  Spawn.update();

  updateCharacter();

  for(var i = 0; i < allActors.length; ++i) {
    var actor = allActors[i];
    if(actor.deleted) {
      actor.remove();
      i--;
    } else {
      actor.update();
    }
  }
  
  actorStage.children.sort(actorSortFunc);

  //Render the stage to see the animation
  renderer.render(superStage);

  updateInput();
}

function scaleRenderer() {
  var style = renderer.view.style;
  var w = window.innerWidth, h = window.innerHeight;
  var aspect = w / h;
  if(aspect < SCR_W/SCR_H) {
    h = (w*SCR_H/SCR_W)|0;
  } else {
    w = (h*SCR_W/SCR_H)|0;
  }
  style.width = w + "px";
  style.height = h + "px";
  style.marginLeft = (-(w>>1)) + 'px';
  style.marginTop = (-(h>>1)) + 'px';
}

function setup() {
  renderer = PIXI.autoDetectRenderer(SCR_W, SCR_H);
  scaleRenderer();

  //Add the canvas to the HTML document
  document.body.appendChild(renderer.view);

  digit_bg = loadAnim("res/number-bg.png", 16, 16);
  digits = loadAnim("res/numbers.png", 16, 16);
  goldDigits = loadAnim("res/numbers-gold.png", 16, 16);
  flashDigits = loadAnim("res/numbers-flash.png", 16, 16);

  var realWindow = window.parent || window;
  realWindow.addEventListener(
    "keydown", function(e)
    {
      activated_keys[e.keyCode] = true;
      keys[e.keyCode] = true;
    }, false
  );
  realWindow.addEventListener(
    "keyup", function(e)
    {
      activated_keys[e.keyCode] = false;
      keys[e.keyCode] = false;
    }, false
  );
  realWindow.addEventListener("resize", scaleRenderer);

  animframes = loadAnim("res/figure-sheet-transparent.png", 32, 32);
  carryframes = loadAnim("res/figure-sheet-carrying.png", 32, 32);

  reset();

  //Start the game loop
  gameLoop();
}

$(document).ready(function(){
  body = $("body");
  
  sounds.load([
    "sounds/explosion.wav",
    "sounds/score.wav",
    "sounds/land.wav",
    "sounds/song1.mp3",
    "sounds/die.wav"
  ]);

  sounds.whenLoaded = function()
  {
    initSounds();
    PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
    PIXI.loader.add([
      "res/bigbox.png",
      "res/smallbox.png",
      "res/smallbox-shadow.png",
      "res/smallbox2.png",
      "res/hatch.png",
      "res/hatch-broken.png",
      "res/light.png",
      "res/bomb.png",
      "res/bomb-shadow.png",
      "res/figure-sheet-transparent.png",
      "res/figure-sheet-carrying.png",
      "res/shadow-16.png",
      "res/env-mockup-3.png",
      "res/enemy-skull.png",
      "res/enemy-skull2.png",
      "res/explosion.png",
      "res/enemy-flying.png",
      "res/number-bg.png",
      "res/numbers.png",
      "res/numbers-gold.png",
      "res/numbers-flash.png",
      "res/player-shadow.png",
      "res/skull-shadow.png",
      "res/bigbox-shadow.png",
    ])
    .load(setup);  
  };
});

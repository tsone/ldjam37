
Array.prototype.erase = function(obj) {
  var i = this.indexOf(obj);
  if(i === -1) return;
  var t = this.pop();
  if(i >= this.length) return;
  this[i] = t;
};

function loadAnim(res, w, h)
{
  var frames = [];
  var t = PIXI.loader.resources[res].texture;
  if(t.frame.height < h || t.frame.width < w) {
    frames[0] = [t];
    return frames;
  }
  for(var y = 0; y <= t.frame.height-h; y += h) {
    frames[y/h] = [];
    for(var x = 0; x <= t.frame.width-w; x += w) {
      var r = new PIXI.Rectangle(x, y, w, h);
      var tex = new PIXI.Texture(t);
      tex.frame = r;
      frames[y/h].push(tex);
    }
  }
  return frames;
}

var startMap = [
9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,
9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,
9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,
9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,
9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,
9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,
9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,
9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,
9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,
9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,
9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,
9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,
9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,
9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,
];

var Level = {

S : 16,
tiles : [],
width : 26,
height : 14,
//1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0
tilemap : startMap.slice(),
container : null,

init : function(stage) {
  var cont = new PIXI.Container();
  Level.container = cont;
  Level.tilemap = startMap.slice();
  cont.x = 0;
  cont.y = 0;
  //stage.addChild(cont);

  //Level.tiles = loadAnim("res/tileset.png", Level.S, Level.S);
},

setPos : function(x, y) {
  Level.container.x = x;
  Level.container.y = y;
},

update : function() {
  Level.container.removeChildren();

  return; // don't show any tiles

  for(var y = 0; y < Level.height; ++y) {
    for(var x = 0; x < Level.width; ++x) {
      var i = Level.tilemap[y*Level.width+x];
      if(!i) continue;
      //var spr = new PIXI.Sprite(Level.tiles[0][i]);
      spr.x = Level.S*x;
      spr.y = Level.S*y;
      //Level.container.addChild(spr);
    }
  }
},

isCollPoint : function(x, y) {
  var tx = Math.floor((x-Level.container.x) / Level.S);
  var ty = Math.floor((y-Level.container.y) / Level.S);
  return (tx >= 0) && (tx < Level.width) && (ty >= 0) && (ty < Level.height)
    && (Level.tilemap[ty*Level.width+tx] > 0);
},

isColl : function(actor) {
  var w = actor.coll.w;
  var h = actor.coll.h;
  return Level.isCollPoint(actor.cx-w, actor.cy-h)
    || Level.isCollPoint(actor.cx+w, actor.cy-h)
    || Level.isCollPoint(actor.cx-w, actor.cy+h)
    || Level.isCollPoint(actor.cx+w, actor.cy+h);
},

constrain : function(actor, oldX, oldY) {
  if(!Level.isColl(actor)) return;
  var t;
    
  t = actor.cy;
  actor.cy = oldY;
  if(Level.isColl(actor)){
    actor.cx = oldX;
    actor.vx = 0;
  }
  actor.cy = t;

  t = actor.cx;
  actor.cx = oldX;
  if(Level.isColl(actor)){
    actor.cy = oldY;
    actor.vy = 0;
  }
  actor.cx = t;
},

};


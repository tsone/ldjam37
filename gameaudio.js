var effects = [];
var music;
var dieSound;
var explosionSound;
var scoreSound;
var landSound;

function initSounds()
{
  music = sounds["sounds/song1.mp3"];

  effects.push(explosionSound = sounds["sounds/explosion.wav"]);	
  effects.push(scoreSound = sounds["sounds/score.wav"]);
  effects.push(landSound = sounds["sounds/land.wav"]);
  effects.push(dieSound = sounds["sounds/die.wav"]);

  explosionSound.volume = 0.5;
  scoreSound.volume = 0.5;
  dieSound.volume = 0.5;

  music.loop = true;
  music.volume = 0.5;
  music.play();
}